
# Stop the mail service
echo "Stoping Sendmail"
systemctl stop postfix

# Remove all existing mail.
# f the mail service wasn't working, there could be 1000s of them
echo "Clearing out existing mail queues"
rm -f /var/spool/mqueue/*


# Start sendmail, this script also compiles /etc/aliaeses
echo "Starting Sendmail"
systemctl start postfix

echo "All Done"