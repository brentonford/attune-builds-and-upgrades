# CHECK HOSTS FILE
# We are looking for this pattern
#
# 192.168.1.1 prodapp1 prodapp1.company.com
# PC_APP_IP PC_APP_HOSTNAME PC_APP_HOSTNAME.PC_DOMAIN_NAME

# Make sure our host file has our FQN at the end.
echo "Checking for line ending with {attuneNode.fqn} in /etc/hosts"
grep "{attuneNode.fqn}$" /etc/hosts

echo "Checking for the SMTP server in the hosts file"
grep "{smtpServer.fqn}$" /etc/hosts