DIR="$HOME/.local/share/applications/"
FILE="jetbrains-pycharm.desktop"

[ -d $DIR ] || mkdir -p $DIR

cat > $DIR$FILE <<EOF
[Desktop Entry]
Version=1.0
Type=Application
Name=PyCharm Professional Edition
Icon=$HOME/bin/pycharm-2021.2.3/bin/pycharm.svg
Exec="$HOME/bin/pycharm-2021.2.3/bin/pycharm.sh" %f
Comment=Python IDE for Professional Developers
Categories=Development;IDE;
Terminal=false
StartupWMClass=jetbrains-pycharm
StartupNotify=true

EOF
