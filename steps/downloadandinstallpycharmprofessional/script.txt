[ -d $HOME/Downloads ] || mkdir -p $HOME/Downloads

pushd $HOME/Downloads > /dev/null

# installer
wget https://download.jetbrains.com/python/pycharm-professional-2021.2.3.tar.gz -O pycharm-professional-2021.2.3.tar.gz

# checksum
wget https://download.jetbrains.com/python/pycharm-professional-2021.2.3.tar.gz.sha256 -O pycharm-professional-2021.2.3.tar.gz.sha256

# check integrity
sha256sum -c pycharm-professional-2021.2.3.tar.gz.sha256

[ -d $HOME/bin ] || mkdir -p $HOME/bin

tar xzvf pycharm-professional-2021.2.3.tar.gz -C $HOME/bin --skip-old-files
