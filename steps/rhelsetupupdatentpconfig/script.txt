F=/etc/sysconfig/ntpdate

# Add -x to the startup uptions
sed -i 's/OPTIONS="-u/OPTIONS="-x -u/g' $F

# Set to 'yes' to sync hw clock after successful ntpdate
sed -i 's/SYNC_HWCLOCK=no/SYNC_HWCLOCK=yes/g' $F
