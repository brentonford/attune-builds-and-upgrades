$VM = '{targetServer.name}'

cd "{virtualHardDiskFolder}"
Write-Host "We are in {virtualHardDiskFolder}."
    
$DISKS = Get-VMHardDiskDrive -VMName $VM
$i = 0
foreach ($DISK in $DISKS) {

    # Keep the first disk which will have ControllerLocation = 0 
    if ($DISK."ControllerLocation" -gt 0) {
        Write-Host "$i. Removing disk $($DISK.ControllerLocation)"
        Remove-VMHardDiskDrive -VMName $VM `
            -ControllerType $DISK."ControllerType" `
            -ControllerNumber $DISK."ControllerNumber" `
            -ControllerLocation $DISK."ControllerLocation"
    }
    
    $i++
}

# The added disk (if any) is number 2 (1 is the original disk).
$DISK_NUMBER = 2

for ($i = 0; $i -lt 3; $i=$i+1) {
    
    # Path of the VM's virtual disk
    # The full path will look something like 
    # "C:\ProgramData\Microsoft\Windows\Virtual Hard Disks{targetServer.name}-disk1.vhdx"
    $path_to_disk = "$VM-disk$DISK_NUMBER.vhdx"
    
    if (-not(Test-Path "$path_to_disk")) {
        Write-Host "$path_to_disk does not exist, skipping."
    } else {
        Write-Host "$path_to_disk exists, deleting it."
        Remove-Item "$path_to_disk"
    }
    
    $DISK_NUMBER++
}