



[![Docs](https://img.shields.io/badge/docs-latest-brightgreen.svg)](http://doc.servertribe.com)
[![Discord](https://img.shields.io/discord/844971127703994369)](http://discord.servertribe.com)
[![Docs](https://img.shields.io/badge/videos-watch-brightgreen.svg)](https://www.youtube.com/@servertribe)
[![Generic badge](https://img.shields.io/badge/download-latest-brightgreen.svg)](https://www.servertribe.com/community-edition/)

# Attune Builds and Upgrades 






# Attune

[Attune](https://www.servertribe.com/)
automates and orchestrates processes to streamline deployments, scaling,
migrations, and management of your systems. The Attune platform is building a
community of sharable automated and orchestrated processes.

You can leverage the publicly available orchestrated blueprints to increase
your productivity, and accelerate the delivery of your projects. You can
open-source your own work and improve existing community orchestrated projects.

## Get Started with Attune, Download NOW!

The **Attune Community Edition** can be
[downloaded](https://www.servertribe.com/comunity-edition/)
for free from our
[ServerTribe website](https://www.servertribe.com/comunity-edition/).
You can learn more about Attune through
[ServerTribe's YouTube Channel](https://www.youtube.com/@servertribe).







# Clone this Project

To clone this project into your own instance of Attune, follow the
[Clone a GIT Project How To Instructions](https://servertribe-attune.readthedocs.io/en/latest/howto/design_workspace/clone_project.html).




## Blueprints

This Project contains the following Blueprints.



### Attune v5 SWPE Install C wimlib


### Attune v5 SWPES Setup Samba


### Backup Attune v4


### Build Attune v5 on Hyper-V


### Build Attune v5 Server on CentOS and Parallels on macOS


### Build Attune v5 Server on ESXi


### Build Attune v5 Server on RHEL8.7+HyperV


### Build Attune Worker Server on ESXi


### Install RHEL Attune v4.3.13 Server


### Prepare ESXi Virtual Disks for Attune Platform


### RHEL Attune Setup Attune User


### Setup Attune Development Desktop Environment on CentOS8


### Setup Attune Platform and Install Attune v5 Software


### UA Clean Up Tar Files and Folders


### Upgrade Attune v3 to Attune v4.3.13


### Upgrade Attune v4.x to Attune v5


### Upgrade Attune v5.x.x to v5.x.x





## Parameters


| Name | Type | Script Reference | Comment |
| ---- | ---- | ---------------- | ------- |
| AD Full Domain Name | Text | `adfulldomainname` |  |
| Attune Kickstart Base Dir | Text | `attunekickstartbasedir` | This is the parameter for the base directory on the Attune node for storing kickstart files. |
| Attune Node | Linux/Unix Node | `attunenode` | This is the target node that we want to build Attune on. |
| Attune Node Admin Email | Text | `attunenodeadminemail` | Sendmail alias |
| Attune Node Service User | Linux/Unix Credential | `attunenodeserviceuser` |  |
| Attune Node User: attune | Linux/Unix Credential | `attunenodeuserattune` |  |
| Attune Node User: root | Linux/Unix Credential | `attunenodeuserroot` |  |
| Attune Server | Linux/Unix Node | `attuneserver` |  |
| Attune Subnet | Network IPv4 Subnet | `attunesubnet` |  |
| Attune Web User: admin | Basic Credential | `attunewebuseradmin` | This is the local credentials used to login to the Attune web page. |
| CMake Ver | Text | `cmakever` |  |
| Environment Servers | Node List | `environmentservers` |  |
| HyperV Host | Windows Node | `hypervhost` |  |
| HyperV Host User | Windows Credential | `hypervhostuser` |  |
| Linux: Attune User | Linux/Unix Credential | `linuxattuneuser` |  |
| Linux: Root User | Linux/Unix Credential | `linuxrootuser` |  |
| NTP Servers | Node List | `ntpservers` |  |
| PostGreSQL Service User | Basic Credential | `postgresqlserviceuser` |  |
| PostgreSQL Ver | Text | `postgresqlver` |  |
| Python Ver | Text | `pythonver` |  |
| RPM Mirror | Linux/Unix Node | `rpmmirror` |  |
| RPM Mirrors Enabled | Text | `rpmmirrorsenabled` | Accept values are '1' for enabled or '0' for disabled. |
| SMTP Server | Basic Node | `smtpserver` |  |
| Target Server | Basic Node | `targetserver` |  |
| Target Server: Lin | Linux/Unix Node | `targetserverlin` | The target server is a generic placeholder, usually used for the server a script will run on.<br>For example, the server being built if the procedure is building a server. |
| TimescaleDB Ver | Text | `timescaledbver` |  |
| /var LVM Volume Size | Text | `varlvmvolumesize` | Size in MB of /var logical volume |
| vCenter Server | Basic Node | `vcenterserver` | Node details for the vCenter Server. |
| vCenter User | Basic Credential | `vcenteruser` | vCenter Credentials. |
| Virtual Hard Disk Folder | Text | `virtualharddiskfolder` |  |
| Worker Node | Linux/Unix Node | `workernode` | This variable is used in the "Kickstart" build procedures, so the "Attune Server" can be used to build Attune servers. |
| Worker User | Linux/Unix Credential | `workeruser` |  |




## Files

| Name | Type | Comment |
| ---- | ---- | ------- |
| Attune Release v4.3.13 | Large Archives |  |
| Attune Release v4 Upgrade Data | Large Archives |  |
| Attune Release v5 | Large Archives |  |
| CMake Source v3.19.2 | Large Archives |  |
| etc Configs | Version Controlled Files |  |
| NTP Config | Version Controlled Files |  |
| Offline Python 3.9.1 Source | Large Archives |  |
| Offline Python PyPIs for Attune | Large Archives | https://pypi.org/project/distlib/#files<br>https://pypi.org/project/filelock/#files<br>https://pypi.org/project/pip/#files<br>https://pypi.org/project/platformdirs/#files<br>https://pypi.org/project/virtualenv/#files<br>https://pypi.org/project/wheel/#files |
| Offline sqlite-devel | Large Archives |  |
| Offline xz-libs | Large Archives |  |
| PostgreSQL Source v12.5 | Large Archives |  |
| Powershell v6.2.4 | Large Archives | https://github.com/PowerShell/PowerShell/releases<br><br>https://github.com/PowerShell/PowerShell/releases/tag/v6.2.4 |
| RHEL yum repos | Version Controlled Files |  |
| TimescaleDB Source v1.7.4 | Large Archives | https://github.com/timescale/timescaledb/archive/refs/tags/1.7.4.tar.gz |
| VMWare.PowerCLI v6.7 | Large Archives | This was downloaded with :<br>sudo pwsh -Command "Save-Module -name VMware.PowerCLI -Path /root" |






# Contribute to this Project

**The collective power of a community of talented individuals working in
concert delivers not only more ideas, but quicker development and
troubleshooting when issues arise.**

If you’d like to contribute and help improve these projects, please fork our
repository, commit your changes in Attune, push you changes, and create a
pull request.

<img src="https://www.servertribe.com/wp-content/uploads/2023/02/Attune-pull-request-01.png" alt="pull request"/>

---

Please feel free to raise any issues or questions you have.

<img src="https://www.servertribe.com/wp-content/uploads/2023/02/Attune-get-help-02.png" alt="create an issue"/>


---

**Thank you**
